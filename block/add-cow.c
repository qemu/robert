#include "qemu-common.h"
#include "block_int.h"
#include "module.h"

#define ADD_COW_MAGIC  (((uint64_t)'A' << 56) | ((uint64_t)'D' << 48) | \
                        ((uint64_t)'D' << 40) | ((uint64_t)'_' << 32) | \
                        ((uint64_t)'C' << 24) | ((uint64_t)'O' << 16) | \
                        ((uint64_t)'W' << 8) | 0xFF)
#define ADD_COW_VERSION 1

struct add_cow_header {
    uint64_t magic;
    uint32_t version;
    char backing_file[1024];
    char image_file[1024];
    uint64_t size;
    uint32_t sectorsize;
}add_cow_header;

typedef struct BDRVAddCowState {
    CoMutex lock;
} BDRVAddCowState;

typedef struct AddCowAIOCB {
    BlockDriverAIOCB common;
    int64_t sector_num;
    QEMUIOVector *qiov;
    uint8_t *buf;
    void *orig_buf;
    int nb_sectors;
    int n;
    struct iovec hd_iov;
    bool is_write;
    QEMUBH *bh;
    QEMUIOVector hd_qiov;
} AddCowAIOCB;

static int add_cow_probe(const uint8_t *buf, int buf_size, const char *filename)
{
    const struct add_cow_header *add_cow_header = (const void *)buf;

    if (buf_size >= sizeof(struct add_cow_header) &&
        be64_to_cpu(add_cow_header->magic) == ADD_COW_MAGIC &&
        be32_to_cpu(add_cow_header->version) == ADD_COW_VERSION) {
        return 100;
    } else {
        return 0;
    }
}

static int add_cow_open(BlockDriverState *bs, int flags)
{
    struct add_cow_header add_cow_header;
    int bitmap_size;
    int64_t size;

    if (bdrv_pread(bs->file, 0, &add_cow_header, sizeof(add_cow_header)) !=
            sizeof(add_cow_header)) {
        goto fail;
    }

    if (be64_to_cpu(add_cow_header.magic) != ADD_COW_MAGIC ||
        be32_to_cpu(add_cow_header.version) != ADD_COW_VERSION) {
        goto fail;
    }

    size = be64_to_cpu(add_cow_header.size);
    bs->total_sectors = size / 512;

    pstrcpy(bs->backing_file, sizeof(bs->backing_file),
            add_cow_header.backing_file);
    pstrcpy(bs->image_file, sizeof(bs->image_file),
            add_cow_header.image_file);

    bitmap_size = ((bs->total_sectors + 7) >> 3) + sizeof(add_cow_header);
    return 0;
 fail:
    return -1;
}

static inline int add_cow_set_bit(BlockDriverState *bs, int64_t bitnum)
{
    uint64_t offset = sizeof(struct add_cow_header) + bitnum / 8;
    uint8_t bitmap;
    int ret;
    ret = bdrv_pread(bs->file, offset, &bitmap, sizeof(bitmap));

    if (ret < 0) {
        return ret;
    }

    bitmap |= (1 << (bitnum % 8));
    ret = bdrv_pwrite_sync(bs->file, offset, &bitmap, sizeof(bitmap));
    if (ret < 0) {
        return ret;
    }
    return 0;
}

static inline int is_bit_set(BlockDriverState *bs, int64_t bitnum)
{
    uint64_t offset = sizeof(struct add_cow_header) + bitnum / 8;
    uint8_t bitmap;
    int ret;
    BDRVAddCowState *s = bs->opaque;
    qemu_co_mutex_unlock(&s->lock);
    ret = bdrv_pread(bs->file, offset, &bitmap, sizeof(bitmap));
    qemu_co_mutex_lock(&s->lock);
    if (ret < 0) {
        return ret;
    }

    return !!(bitmap & (1 << (bitnum % 8)));
}

static int add_cow_is_allocated(BlockDriverState *bs, int64_t sector_num,
        int nb_sectors, int *num_same)
{
    int changed;

    if (nb_sectors == 0) {
        *num_same = nb_sectors;
        return 0;
    }

    changed = is_bit_set(bs, sector_num);
    if (changed < 0) {
        return 0;
    }

    for (*num_same = 1; *num_same < nb_sectors; (*num_same)++) {
        if (is_bit_set(bs, sector_num + *num_same) != changed) {
            break;
        }
    }

    return changed;
}

static int add_cow_update_bitmap(BlockDriverState *bs, int64_t sector_num,
        int nb_sectors)
{
    int error = 0;
    int i;

    for (i = 0; i < nb_sectors; i++) {
        error = add_cow_set_bit(bs, sector_num + i);
        if (error) {
            break;
        }
    }

    return error;
}

static void add_cow_close(BlockDriverState *bs)
{
}

static int add_cow_create(const char *filename, QEMUOptionParameter *options)
{
    struct add_cow_header add_cow_header;
    int64_t image_sectors = 0;
    const char *backing_filename = NULL;
    const char *image_filename = NULL;
    int ret;
    BlockDriverState* bs;

    while (options && options->name) {
        if (!strcmp(options->name, BLOCK_OPT_SIZE)) {
            image_sectors = options->value.n / 512;
        } else if (!strcmp(options->name, BLOCK_OPT_BACKING_FILE)) {
            backing_filename = options->value.s;
        } else if (!strcmp(options->name, BLOCK_OPT_IMAGE_FILE)) {
            image_filename = options->value.s;
        }
        options++;
    }
    if (!backing_filename || !image_filename) {
        fprintf(stderr, " backing_file and image_file can not be empty!\n");
        return -EINVAL;
    }
    ret = bdrv_create_file(filename, NULL);
    if (ret < 0) {
        return ret;
    }

    ret = bdrv_file_open(&bs, filename, BDRV_O_RDWR);
    if (ret < 0) {
        return ret;
    }

    memset(&add_cow_header, 0, sizeof(add_cow_header));
    add_cow_header.magic = cpu_to_be64(ADD_COW_MAGIC);
    add_cow_header.version = cpu_to_be32(ADD_COW_VERSION);
    pstrcpy(add_cow_header.backing_file, \
                sizeof(add_cow_header.backing_file), backing_filename);
    pstrcpy(add_cow_header.image_file, sizeof(add_cow_header.image_file),
                image_filename);

    add_cow_header.sectorsize = cpu_to_be32(512);
    add_cow_header.size = cpu_to_be64(image_sectors * 512);

    ret = bdrv_pwrite(bs, 0, &add_cow_header, sizeof(add_cow_header));
    if (ret < 0) {
        return ret;
    }
    bdrv_close(bs);

    ret = bdrv_create_file(image_filename, NULL);
    if (ret < 0) {
        return ret;
    }

    BlockDriver* drv = bdrv_find_format("add-cow");
    assert(drv != NULL);
    ret = bdrv_open(bs, filename, BDRV_O_RDWR | BDRV_O_NO_FLUSH, drv);
    if (ret < 0) {
        return ret;
    }

    ret = bdrv_truncate(bs, ((image_sectors + 7) >> 3));
    if (ret < 0) {
        return ret;
    }
    return ret;
}

static void add_cow_aio_cancel(BlockDriverAIOCB *blockacb)
{
    AddCowAIOCB *acb = container_of(blockacb, AddCowAIOCB, common);
    qemu_aio_release(acb);
}

static AIOPool add_cow_aio_pool = {
    .aiocb_size         = sizeof(AddCowAIOCB),
    .cancel             = add_cow_aio_cancel,
};

static AddCowAIOCB *add_cow_aio_setup(BlockDriverState *bs,
        int64_t sector_num, QEMUIOVector *qiov, int nb_sectors,
        int is_write)
{
    AddCowAIOCB *acb;
    acb = qemu_aio_get(&add_cow_aio_pool, bs, NULL, NULL);
    if (!acb) {
        return NULL;
    }
    acb->sector_num = sector_num;
    acb->qiov = qiov;
    acb->is_write = is_write;

    if (qiov->niov > 1) {
        acb->buf = acb->orig_buf = qemu_blockalign(bs, qiov->size);
        if (is_write) {
            qemu_iovec_to_buffer(qiov, acb->buf);
        }
    } else {
        acb->buf = (uint8_t *)qiov->iov->iov_base;
    }
    acb->nb_sectors = nb_sectors;
    acb->n = 0;
    return acb;
}

static int add_cow_aio_read_cb(void *opaque)
{
    AddCowAIOCB *acb = opaque;
    BlockDriverState *bs = acb->common.bs;
    BDRVAddCowState *s = bs->opaque;
    int ret;
    int n;
    acb->nb_sectors -= acb->n;
    acb->sector_num += acb->n;
    acb->buf += acb->n * 512;

    if (acb->nb_sectors == 0) {
        /* request completed */
        return 0;
    }
    acb->n = acb->nb_sectors;
    if (add_cow_is_allocated(bs, acb->sector_num, acb->n, &n)) {
        acb->hd_iov.iov_base = (void *)acb->buf;
        acb->hd_iov.iov_len = n * 512;
        qemu_iovec_init_external(&acb->hd_qiov, &acb->hd_iov, 1);
        qemu_co_mutex_unlock(&s->lock);
        ret = bdrv_co_readv(bs->cow_hd, acb->sector_num,
                            n, &acb->hd_qiov);
        qemu_co_mutex_lock(&s->lock);
        if (ret < 0) {
            return -EIO;
        }
        acb->n = n;
        return 1;
    } else {
        acb->n = n;
        if (bs->backing_hd) {
            acb->hd_iov.iov_base = (void *)acb->buf;
            acb->hd_iov.iov_len = n * 512;
            qemu_iovec_init_external(&acb->hd_qiov, &acb->hd_iov, 1);
            qemu_co_mutex_unlock(&s->lock);
            ret = bdrv_co_readv(bs->backing_hd, acb->sector_num,
                                n, &acb->hd_qiov);
            qemu_co_mutex_lock(&s->lock);
            if (ret < 0) {
                return -EIO;
            }
            return 1;
        } else {
            memset(acb->buf, 0, 512 * n);
            return 1;
        }
        acb->n = n;
    }

    return 1;
}

static int add_cow_aio_write_cb(void *opaque)
{
    AddCowAIOCB *acb = opaque;
    BlockDriverState *bs = acb->common.bs;
    BDRVAddCowState *s = bs->opaque;
    int ret = 0;

    acb->nb_sectors -= acb->n;
    acb->sector_num += acb->n;
    acb->buf += acb->n * 512;

    if (acb->nb_sectors == 0) {
        return 0;
    }

    acb->n = acb->nb_sectors;

    acb->hd_iov.iov_base = (void *)acb->buf;
    acb->hd_iov.iov_len = acb->n * 512;
    qemu_iovec_init_external(&acb->hd_qiov, &acb->hd_iov, 1);

    qemu_co_mutex_unlock(&s->lock);
    ret = bdrv_co_writev(bs->cow_hd,
                         acb->sector_num,
                         acb->n, &acb->hd_qiov);
    qemu_co_mutex_lock(&s->lock);

    if (ret < 0) {
        return ret;
    }
    return 1;
}

static int add_cow_co_readv(BlockDriverState *bs, int64_t sector_num,
                         int nb_sectors, QEMUIOVector *qiov)
{
    BDRVAddCowState *s = bs->opaque;
    AddCowAIOCB *acb;
    int ret;

    acb = add_cow_aio_setup(bs, sector_num, qiov, nb_sectors, 0);
    qemu_co_mutex_lock(&s->lock);
    do {
        ret = add_cow_aio_read_cb(acb);
    } while (ret > 0);
    qemu_co_mutex_unlock(&s->lock);

    if (acb->qiov->niov > 1) {
        qemu_iovec_from_buffer(acb->qiov, acb->orig_buf, acb->qiov->size);
        qemu_vfree(acb->orig_buf);
    }
    qemu_aio_release(acb);

    return ret;
}

static int add_cow_co_writev(BlockDriverState *bs, int64_t sector_num,
                          int nb_sectors, QEMUIOVector *qiov)
{
    BDRVAddCowState *s = bs->opaque;
    AddCowAIOCB *acb;
    int ret;

    acb = add_cow_aio_setup(bs, sector_num, qiov, nb_sectors, 1);

    qemu_co_mutex_lock(&s->lock);
    do {
        ret = add_cow_aio_write_cb(acb);
    } while (ret > 0);
    qemu_co_mutex_unlock(&s->lock);

    if (acb->qiov->niov > 1) {
        qemu_vfree(acb->orig_buf);
    }
    qemu_aio_release(acb);

    if(ret == 0) {
        add_cow_update_bitmap(bs, sector_num, nb_sectors);
    }
    return ret;
}

static int bdrv_add_cow_truncate(BlockDriverState *bs, int64_t offset)
{
    int ret;
    ret = bdrv_truncate(bs->file,offset + sizeof(add_cow_header));
    if (ret < 0) {
        return ret;
    }

    return 0;
}

static QEMUOptionParameter add_cow_create_options[] = {
    {
        .name = BLOCK_OPT_SIZE,
        .type = OPT_SIZE,
        .help = "Virtual disk size"
    },
    {
        .name = BLOCK_OPT_BACKING_FILE,
        .type = OPT_STRING,
        .help = "File name of a base image"
    },
    {
        .name = BLOCK_OPT_IMAGE_FILE,
        .type = OPT_STRING,
        .help = "File name of a image file"
    },
    { NULL }
};

static int add_cow_flush(BlockDriverState *bs)
{
    return bdrv_flush(bs->file);
}

static BlockDriverAIOCB *add_cow_aio_flush(BlockDriverState *bs,
        BlockDriverCompletionFunc *cb, void *opaque)
{
    return bdrv_aio_flush(bs->file, cb, opaque);
}

static BlockDriver bdrv_add_cow = {
    .format_name        = "add-cow",
    .instance_size      = sizeof(BDRVAddCowState),
    .bdrv_probe         = add_cow_probe,
    .bdrv_open          = add_cow_open,
    .bdrv_close         = add_cow_close,
    .bdrv_create        = add_cow_create,
    .bdrv_is_allocated  = add_cow_is_allocated,

    .bdrv_co_readv      = add_cow_co_readv,
    .bdrv_co_writev     = add_cow_co_writev,
    .bdrv_truncate      = bdrv_add_cow_truncate,

    .create_options     = add_cow_create_options,
    .bdrv_flush         = add_cow_flush,
    .bdrv_aio_flush     = add_cow_aio_flush,
};

static void bdrv_add_cow_init(void)
{
    bdrv_register(&bdrv_add_cow);
}

block_init(bdrv_add_cow_init);
